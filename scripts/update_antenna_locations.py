# -*- coding: utf-8 -*-
"""
A script to update the antenna locations.

It reads in a pre-existing aavs3.yaml file,
and a CSV file of antenna locations taken from
https://confluence.skatelescope.org/display/SE/MCCS-1826%3A+Provide+AAVS3+data+for+telmodel+export
and generates YAML with updated locations.

This is most likely a one-shot script that will never be run again.
But let's check it in anyhow, as a record of what was done.
"""
import csv
import yaml


with open('scripts/antenna_export_w2.csv') as csvfile:
    reader = csv.DictReader(csvfile)

    by_id = {}
    for row in reader:
        station_id = row["station_id"]
        x = float(row["x_pos"])
        y = float(row["y_pos"])
        z = float(row["z_pos"])
        by_id[station_id] = (x, y, z)

with open("helmfile.d/values/aavs3.yaml", "r") as f:
    platform_config = yaml.load(f, Loader=yaml.SafeLoader)
    antenna_config = platform_config["platform"]["array"]["station_clusters"]["a1"]["stations"]["1"]["antennas"]

    for antenna_spec in antenna_config.values():
        smartbox = antenna_spec["smartbox"]
        smartbox_port = antenna_spec["smartbox_port"]
        station_id = f"SB{smartbox}-{smartbox_port}"
        (x, y, z) = by_id[station_id]

        antenna_spec["location_offset"]["east"] = round(x, 3)
        antenna_spec["location_offset"]["north"] = round(y, 3)
        antenna_spec["location_offset"]["up"] = round(z, 3)

print(yaml.dump(platform_config, sort_keys=False))

