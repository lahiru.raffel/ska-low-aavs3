include .make/base.mk

########################################
## Landing website

install-pelican:
	python3 -m pip install pelican tzdata

site-build:
	cd site; pelican content/

site-build-for-publish:
	cd site; pelican --settings publishconf.py content/

site-launch:
	cd site; pelican --listen


########################################
## OCI

include .make/oci.mk

oci-pre-build: install-pelican site-build-for-publish


########################################
## k8s / Helm

include .make/k8s.mk
include .make/helm.mk
include .make/tmdata.mk

export HELM_KUBECONTEXT=infra:au-itf-aavs302-k8s

deploy-aavs3: K8S_UMBRELLA_CHART_PATH=charts/ska-low-aavs3
deploy-aavs3: HELM_RELEASE=ska-low-aavs3
deploy-aavs3: KUBE_NAMESPACE=aavs3-system
deploy-aavs3: K8S_CHARTS=ska-low-aavs3
deploy-aavs3: k8s-install-chart

deploy-mccs: K8S_UMBRELLA_CHART_PATH=charts/ska-low-aavs3-mccs
deploy-mccs: HELM_RELEASE=ska-low-mccs
deploy-mccs: KUBE_NAMESPACE=ska-low-mccs
deploy-mccs: K8S_CHARTS=ska-low-aavs3-mccs
deploy-mccs: k8s-install-chart
