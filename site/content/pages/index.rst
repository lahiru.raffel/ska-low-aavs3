The AAVS3 is currently in commissioning.
########################################

:save_as: index.html

This is the landing page for SKAO's Aperture Array Verification System 3 (AAVS3).

The AAVS3 is currently in commissioning,
and is not yet available for science observations.

Resources
~~~~~~~~~

* `AAVS2/3 calendar`_
* `Jupyterhub </jupyterhub/>`_ (provide dummy values to username / password fields)
* `Taranta </aavs3-system/taranta/devices>`_
  (see `Taranta users`_ for credentials; select user ``DEFAULT`` if no other user applies)

Monitoring tools
................
* `Grafana 1 </grafana/>`_ -- for monitoring the telescope (user ``aavs3``, password ``monk-burgeon-airspeed``)
* `Grafana 2 <http://10.132.47.3:3000/grafana/>`_ -- for monitoring the platform
* `Zabbix <http://172.16.0.233/zabbix/zabbix.php?action=dashboard.view>`_
  -- mostly for monitoring the network
  (user ``Skaguest``, password ``Silent-Surplus95``)

Most of the above links are only accessible when on the SKAO network.
Access via SKAO VPN is restricted by default, but may be granted on request.

Access
~~~~~~
Access to AAVS3 is authorised by Marco Caiazzo.

Before using AAVS3, you must book time on the `AAVS2/3 calendar`_.
Book in advance and for a reasonable amount of time.
The calendar is moderated by Marco Caiazzo.

.. image:: images/aavs3-under-construction.jpeg
  :width: 800
  :align: center
  :alt: AAVS3 under construction

.. _AAVS2/3 calendar: https://confluence.skatelescope.org/display/LFAA/calendar/d1c6a110-7406-4a0f-bb4f-c623cc7e636e
.. _Taranta users: https://developer.skao.int/projects/ska-tango-taranta-suite/en/latest/taranta_users.html
